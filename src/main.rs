use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

// Data structure to represent a coin flip result to be stored in DynamoDB
#[derive(Debug, Serialize, Deserialize)]
pub struct FlipResult {
    pub id: String,  // Unique identifier for the flip
    pub outcome: String, // Outcome of the flip, either "Heads" or "Tails"
}

// Structure for error responses
#[derive(Debug, Serialize)]
struct ErrorResponse {
    pub message: String,
}

// Implementation of Display for the ErrorResponse
impl std::fmt::Display for ErrorResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

// Represents incoming requests, specifying whether to add a new flip result or view past results
#[derive(Deserialize)]
struct FlipRequest {
    command: String, // Either "add" or "view"
}

// Structure for sending back responses
#[derive(Serialize)]
struct FlipResponse {
    request_id: String,
    message: String,
}

// Main handler function for adding or viewing flip results
async fn flip_handler(event: LambdaEvent<FlipRequest>) -> Result<FlipResponse, Error> {
    let request_id = event.context.request_id;
    let mut rng = rand::thread_rng();
    let flip: u8 = rng.gen_range(0..=1);
    let outcome_text = if flip == 0 { "Heads" } else { "Tails" };

    let aws_config = aws_config::load_from_env().await;
    let dynamodb_client = Client::new(&aws_config);

    let flip_result = FlipResult {
        id: request_id.clone(),
        outcome: String::from(outcome_text),
    };

    let flip_id = AttributeValue::S(flip_result.id.clone());
    let flip_outcome = AttributeValue::S(flip_result.outcome);

    // Store the flip result in the DynamoDB table named "FlipResults"
    let _ = dynamodb_client
        .put_item()
        .table_name("FlipResults")
        .item("id", flip_id)
        .item("outcome", flip_outcome)
        .send()
        .await
        .map_err(|err| ErrorResponse {
            message: err.to_string(),
        });

    let response = FlipResponse {
        request_id,
        message: format!("Result of the flip: {}.", outcome_text),
    };

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(flip_handler)).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::{Context, LambdaEvent};

    #[tokio::test]
    async fn response_valid_for_simple_input() {
        let test_id = "test123";

        let mut context = Context::default();
        context.request_id = test_id.to_string();

        let test_request = FlipRequest {
            command: "add".to_string(),
        };
        let event = LambdaEvent { payload: test_request, context };

        let result = flip_handler(event).await.unwrap();

        assert_eq!(result.request_id, test_id);
    }
}
